#!/bin/bash
# make apt slient if you want
function apt {  /usr/bin/apt "$@" >/dev/null; }
set -e
EXTRA_CRYPTO=$1
ABI=$2
[ -d "$EXTRA_CRYPTO" ] || {
    echo "OPENSSL TARGET DIR IS REQUIRED"
    exit 1
}

BUILD_DIR=${BUILD_DIR:-"./"}
OPENSSL_VERSION=${OPENSSL_VERSION:-"OpenSSL_1_1_1q"}
OPENSSL_BUILD_DIR="${BUILD_DIR}/openssl"

command -v "${ABI}-gcc" || apt -qq install gcc-"${ABI}" -y
command -v wget || apt -qq install wget -y

if [ ! -d "$EXTRA_CRYPTO""/include" ]; then
    rm -rf "$EXTRA_CRYPTO"
    mkdir -p "$EXTRA_CRYPTO"
    if [ ! -d "$OPENSSL_BUILD_DIR" ]; then
        rm -rf "$OPENSSL_BUILD_DIR"
        git clone --depth 1 -b "$OPENSSL_VERSION" https://github.com/openssl/openssl "$OPENSSL_BUILD_DIR"
    fi
    case ${ABI} in
    x86-64-* | x86_64-*)
        osTap=linux-x86_64
        ;;
    i686-*)
        osTap=linux-x86
        ;;
    arm-*)
        osTap=linux-generic32
        ;;
    aarch64-*)
        osTap=linux-aarch64
        ;;
    mips-* | mipsel-*)
        osTap=linux-mips32
        ;;
    mips64-* | mips64el-*)
        osTap=linux64-mips64
        ;;
    *)
        echo "Unknown OPENSSL OS/ARCH configuration"
        exit 1
        ;;
    esac
    cd "$OPENSSL_BUILD_DIR"
    ./Configure --prefix=/ --cross-compile-prefix="$ABI-" no-shared no-dso $osTap
    make -j $(nproc) >/dev/null || make -j 1
    make DESTDIR="$EXTRA_CRYPTO" install
    make distclean
fi
