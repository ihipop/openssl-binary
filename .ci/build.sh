#!/bin/bash
# make apt slient if you want
function apt {  /usr/bin/apt "$@" >/dev/null; }
set -e
SELF_DIR=$(readlink -f "$(dirname "$0")")
echo "$SELF_DIR"

######### set up ABIS
apt -qq -o "Acquire::https::Verify-Peer=false" update && apt -qq -o "Acquire::https::Verify-Peer=false" install -y ca-certificates
apt -qq install build-essential git -y
defaultTargetAbis="x86_64-linux-gnu,i686-linux-gnu,\
aarch64-linux-gnu,arm-linux-gnueabi,arm-linux-gnueabihf,\
mips-linux-gnu,mips64-linux-gnuabi64,mipsel-linux-gnu,mips64-linux-gnuabi64,mips64el-linux-gnuabi64"
TARGET_ABIS=${TARGET_ABIS:-$defaultTargetAbis}
hostABI=$(gcc -dumpmachine)
echo "HOST ABI is:" "$hostABI"
[[ "$hostABI" == "x86_64"* ]] || (
    echo "HOST ABI must be x86_64 !!!"
    exit 1
)

command -v  x86_64-linux-gnu-gcc || alias x86_64-linux-gnu-gcc=gcc

echo "selected cross build ABIS: $TARGET_ABIS"

######### CONFIG
BUILD_DIR=${BUILD_DIR:-"/build"}
OUTPUT_DIR=${OUTPUT_DIR:-"${BUILD_DIR}/target-binary/"}
OPENSSL_VERSION=${OPENSSL_VERSION:-"OpenSSL_1_1_1q"}
OPENSSL_BUILD_DIR="${BUILD_DIR}/openssl"
cd "$BUILD_DIR"

IFS=","
for abi in $TARGET_ABIS; do
    IFS=""
    
    command -v "${abi}-gcc" || apt -qq install gcc-"${abi}" -y
    
    EXTRA_CRYPTO="${OUTPUT_DIR}/${abi}"
    mkdir -p  "${EXTRA_CRYPTO}"
    OPENSSL_VERSION=${OPENSSL_VERSION} OPENSSL_BUILD_DIR=${OPENSSL_BUILD_DIR} bash "$SELF_DIR"/build-openssl.sh "${EXTRA_CRYPTO}" "${abi}" || exit 1
done

apt -qq install file tree -y

tree "$OUTPUT_DIR" || exit 0
file "$OUTPUT_DIR"/*/* || exit 0